#include "server.h"
#include <iostream>

server::server(int port)
{
   ListenSocket = INVALID_SOCKET;
   ClientSocket = INVALID_SOCKET;
   result = NULL;
   portno = port;

   ZeroMemory(&hints, sizeof(hints));
   hints.ai_family = AF_INET;
   hints.ai_socktype = SOCK_STREAM;
   hints.ai_protocol = IPPROTO_TCP;
   hints.ai_flags = AI_PASSIVE;

   // Resolve the server address and port
char portstr[50];
sprintf_s(portstr, "%d", portno);
   int n = getaddrinfo(NULL, portstr, &hints, &result);
   if ( n != 0 ) {
       printf("getaddrinfo failed with error: %d\n", n);
       WSACleanup();
   }

    ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
   if (ListenSocket == INVALID_SOCKET) {
       printf("socket failed with error: %ld\n", WSAGetLastError());
       freeaddrinfo(result);
       WSACleanup();
   }

}

int server::bind_to()
{
       // Setup the TCP listening socket
   int n = bind( ListenSocket, result->ai_addr, (int)result->ai_addrlen);
   if (n == SOCKET_ERROR) {
       printf("bind failed with error: %d\n", WSAGetLastError());
       freeaddrinfo(result);
       closesocket(ListenSocket);
       WSACleanup();
       return 1;
   }

   freeaddrinfo(result);
return 0;
}

int server::listen_to()
{
   int n = listen(ListenSocket, SOMAXCONN);
   if (n == SOCKET_ERROR) {
       printf("listen failed with error: %d\n", WSAGetLastError());
       closesocket(ListenSocket);
       WSACleanup();
       return 1;
   }

   std::cout<< "Server starting......." << std::endl;

   // Accept a client socket
   ClientSocket = accept(ListenSocket, NULL, NULL);
   std::cout<< "Server started...." << std::endl;

   if (ClientSocket == INVALID_SOCKET) {
       printf("accept failed with error: %d\n", WSAGetLastError());
       closesocket(ListenSocket);
       WSACleanup();
       return 1;
   }

   // No longer need server socket
   closesocket(ListenSocket);

return 0;
}

int server::recv_from(std::string& str)
{
char recvbuf[MAX_BUF];
memset(&recvbuf, 0, MAX_BUF);
int n = recv(ClientSocket, recvbuf, MAX_BUF, 0);
if (n < 0)
{
printf("recv failed with error %d\n", n);
closesocket(ClientSocket);
WSACleanup();
return 1;
}
str.assign(recvbuf);
return 0;
}

int server::send_to(std::string& str)
{
int n = send(ClientSocket, str.c_str(), str.size(), 0);
if (n == SOCKET_ERROR)
{
printf("Send failed with error %d\n", WSAGetLastError());
closesocket(ClientSocket);
WSACleanup();
return 1;
}
return 0;
}

int server::close()
{
       // shutdown the connection since we're done
   int n = shutdown(ClientSocket, SD_SEND);
   if (n == SOCKET_ERROR) {
       printf("shutdown failed with error: %d\n", WSAGetLastError());
       closesocket(ClientSocket);
       WSACleanup();
       return 1;
   }

   // cleanup
   closesocket(ClientSocket);
   WSACleanup();
return 0;
}