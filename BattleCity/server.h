
#undef UNICODE

#ifndef SERVER_H_INCLUDED
#define SERVER_H_INCLUDED

//#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_PORT 5001
#define MAX_BUF 256

class server
{
   WSADATA wsaData;
   SOCKET ListenSocket, ClientSocket;
   struct addrinfo *result;
   struct addrinfo hints;
   int portno;

public:

   server(int port = DEFAULT_PORT);
   int bind_to();
   int listen_to();
int recv_from(std::string& str);
int send_to(std::string& str);
int close();

};

#endif // SERVER_H_INCLUDED