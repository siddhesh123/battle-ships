#include "client.h"

client::client(const char* host, int portno)
{
ConnectSocket = INVALID_SOCKET;
result = ptr = NULL;

printf("creating client\n");

ZeroMemory(&hints, sizeof(hints));
hints.ai_family = AF_UNSPEC;
hints.ai_socktype = SOCK_STREAM;
hints.ai_protocol = IPPROTO_TCP;

// Resolve the server address and port
char portstr[20];
sprintf_s(portstr, "%d", portno);
int n = getaddrinfo(host, portstr, &hints, &result);
if (n != 0) {
printf("getaddrinfo failed with error: %d\n", n);
WSACleanup();
}
}


int client::connect_to()
{
// Attempt to connect to an address until one succeeds
	int count = 1;
for (ptr = result; ptr != NULL&&count; ptr = ptr->ai_next) {
	count = 0;

// Create a SOCKET for connecting to server

ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
if (ConnectSocket == INVALID_SOCKET) {
printf("socket failed with error: %ld\n", WSAGetLastError());
WSACleanup();
}

// Connect to server.
printf("client connecting\n");
int n = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
if (n == SOCKET_ERROR) {
	printf("socket error!\n");

	closesocket(ConnectSocket);
ConnectSocket = INVALID_SOCKET;
continue;
}
break;
}

freeaddrinfo(result);

if (ConnectSocket == INVALID_SOCKET) {
printf("Unable to connect to server!\n");
WSACleanup();
return 1;
}
return 0;

}


int client::send_to(std::string& str)
{
int n = send(ConnectSocket, str.c_str(), str.size(), 0);
if (n == SOCKET_ERROR) {
printf("send failed with error: %d\n", WSAGetLastError());
closesocket(ConnectSocket);
WSACleanup();
return 1;
}
return 0;
}

int client::close()
{
int n = shutdown(ConnectSocket, SD_SEND);
if (n == SOCKET_ERROR) {
printf("shutdown failed with error: %d\n", WSAGetLastError());
closesocket(ConnectSocket);
WSACleanup();
return 1;
}
closesocket(ConnectSocket);
WSACleanup();
return 0;
}

int client::recv_from(std::string& str)
{
char recvbuf[MAX_BUF];
memset(&recvbuf, 0, MAX_BUF);
int n = recv(ConnectSocket, recvbuf, MAX_BUF, 0);
if (n < 0)
{
printf("recv failed with error: %d\n", WSAGetLastError());
return 1;
}
else
{
str.assign(recvbuf);
return 0;
}
}