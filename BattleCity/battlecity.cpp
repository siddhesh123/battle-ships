#include "battlecity.h"
#include "QtGui\qdialog.h"

BattleCity::BattleCity(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
    WSADATA wsaData;

	int n = WSAStartup(MAKEWORD(2, 2), &wsaData);

	if (n != 0) {
	printf("WSAStartup failed with error: %d\n", n);
	}

	s = new server();
	c = new client("127.0.0.1");

	box = new QBoxLayout(QBoxLayout::LeftToRight);

	connectionWidget = new ConnectionWidget(this);

	box->addWidget(connectionWidget);

	this->setLayout(box);
}

void BattleCity::processHostButton() {
			
	QLabel *waitingLabel = new QLabel(this);
	waitingLabel->setText("Waiting....");
	this->box->addWidget(waitingLabel);
	this->show();

	s->bind_to();
	s->listen_to();

}

void BattleCity::processConnectButton() {
			
	QLabel *waitingLabel = new QLabel(this);
	waitingLabel->setText("Please enter the IP address....");
	this->box->addWidget(waitingLabel);	

	/*
	c->connect_to();
	c->send_to(std::string("Hi, I am the client"));

	c->recv_from(clientReceivedData);
	std::cout << clientReceivedData;
	*/

}

void BattleCity::keyPressEvent(QKeyEvent *event) {

	
	std::string clientReceivedData;
	std::string serverReceivedData;

	printf("The key pressed is %c\n", event->key());
	
	switch(event->key()) {
	
		case 'S':
	
			s->recv_from(serverReceivedData);
			s->send_to(std::string("I got your message"));
			std::cout << serverReceivedData;
	
			break;

		case 'C':

		default:
			break;
	}

}

BattleCity::~BattleCity()
{
	
}
