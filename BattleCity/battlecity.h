#ifndef BATTLECITY_H
#define BATTLECITY_H

#include <QtGui\qmainwindow.h>
#include <QKeyEvent>
#include <QtGui\qlabel.h>


#include "client.h"
#include "server.h"
#include "connectionwidget.h"

#include <iostream>

#include "ui_battlecity.h"

class BattleCity : public QMainWindow
{
	Q_OBJECT

public:
	BattleCity(QWidget *parent = 0);
	~BattleCity();

private:
	Ui::BattleCityClass ui;

	QBoxLayout *box;

	server *s;
	client *c;

	ConnectionWidget *connectionWidget;

public slots:
	void processHostButton();
	void processConnectButton();

protected:
	void keyPressEvent(QKeyEvent *event);

};

#endif // BATTLECITY_H
