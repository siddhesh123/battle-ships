/********************************************************************************
** Form generated from reading UI file 'battlecity.ui'
**
** Created: Fri 20. Dec 21:22:28 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BATTLECITY_H
#define UI_BATTLECITY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BattleCityClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *BattleCityClass)
    {
        if (BattleCityClass->objectName().isEmpty())
            BattleCityClass->setObjectName(QString::fromUtf8("BattleCityClass"));
        BattleCityClass->resize(600, 400);
        menuBar = new QMenuBar(BattleCityClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        BattleCityClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(BattleCityClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        BattleCityClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(BattleCityClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        BattleCityClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(BattleCityClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        BattleCityClass->setStatusBar(statusBar);

        retranslateUi(BattleCityClass);

        QMetaObject::connectSlotsByName(BattleCityClass);
    } // setupUi

    void retranslateUi(QMainWindow *BattleCityClass)
    {
        BattleCityClass->setWindowTitle(QApplication::translate("BattleCityClass", "BattleCity", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class BattleCityClass: public Ui_BattleCityClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BATTLECITY_H
