#include <iostream>
#include "connectionwidget.h"
#include <QtGui\qpushbutton.h>

ConnectionWidget::ConnectionWidget(QWidget *parent)
    : QWidget(parent)
{
	QBoxLayout *box = new QBoxLayout(QBoxLayout::TopToBottom);

	QPushButton *hostButton = new QPushButton();
	hostButton->setText("Host");
	connect(hostButton, SIGNAL(clicked(bool)), this->parent(), SLOT(processHostButton()));

	QPushButton *connectButton = new QPushButton();
	connectButton->setText("Connect");
	connect(connectButton, SIGNAL(clicked(bool)), this->parent(), SLOT(processConnectButton()));

	box->addWidget(hostButton);
	box->addWidget(connectButton);

	this->setFixedSize(100, 100);
	this->setLayout(box);

}

