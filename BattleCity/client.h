#undef UNICODE

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>

#define DEFAULT_PORT 5001
#define MAX_BUF 256

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

class client
{
WSADATA wsaData;
SOCKET ConnectSocket;

struct addrinfo *result, *ptr;
struct addrinfo hints;

public:

client(const char* host, int port=DEFAULT_PORT);
int connect_to();
int send_to(std::string& str);
int recv_from(std::string& str);
int close();

};